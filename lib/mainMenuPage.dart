import 'package:flutter/material.dart';
import 'package:new_final_project/PickedMenuItem.dart';

import 'PickedMenuItem.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text("Menu page"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart, color: Colors.white),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => FirstRoute()),
              );
            },
          ),
        ],
      ),
      body: Center(
        child: Text(
          "Welcome to AniMenu Restaurant\nThis menu has been inspired by Harry Potter and a lot of Anime Shows\n",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 30),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            ListTile(
              title: Text("Drinks"),
              trailing: Icon(Icons.local_drink),
              onTap: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => new Menu("Drinks"),
                  ),
                );
              },
            ),
            ListTile(
              title: Text("Soups"),
              trailing: Icon(Icons.fastfood),
              onTap: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => new Menu("Soups"),
                  ),
                );
              },
            ),
            ListTile(
              title: Text("Meals"),
              trailing: Icon(Icons.fastfood),
              onTap: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => new Menu("Meals"),
                  ),
                );
              },
            ),
            ListTile(
              title: Text("Desserts"),
              trailing: Icon(Icons.fastfood),
              onTap: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => new Menu("Desserts"),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => FirstRoute()),
          );
        },
        label: Text(
          'Click here to see what you ordered',
          style: TextStyle(color: Colors.white),
        ),
        icon: Icon(
          Icons.shopping_cart,
          color: Colors.white,
        ),
        backgroundColor: Colors.deepPurple[700],
      ),
    );
  }
}
