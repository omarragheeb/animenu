import 'package:flutter/material.dart';
import 'package:new_final_project/wrapper.dart';
import 'package:splashscreen/splashscreen.dart';

void main() {
  runApp(new MaterialApp(
    theme: new ThemeData(
      brightness: Brightness.dark,
      primaryColor: Colors.deepPurple[700],
    ),
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 5,
        navigateAfterSeconds: new AfterSplash(),
        title: new Text(
          'Loading AniMenu',
          style: new TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 50.0,
              color: Colors.deepPurple[700]),
        ),
        backgroundColor: Colors.black,
        onClick: () => print("test to c if working"),
        loaderColor: Colors.white);
  }
}

class AfterSplash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: Wrapper(),
      ),
    );
  }
}
