library flutterhortonswithquantity.globals;

int drinkscounter = 0;
int soupscounter = 0;
int mealscounter = 0;
int dessertscounter = 0;

List<int> totaldrinks = [];
//int coffeequantity = 0;
List<int> totalsoups = [];
//int burgerquantity = 0;
List<int> totalmeals = [];
//int pizzaquantity = 0;
int totalprice = 0;
List<int> totaldesserts = [];

final int butterbeervalue = 3;
final int unicornbloodvalue = 5;
final int hitmanshotevalue = 7;
final int narutoramenevalue = 6;
final int creamstewevalue = 8;
final int misosoupevalue = 10;
final int dragonstewvalue = 20;
final int bleachcutvalue = 22;
final int killermackerelvalue = 18;
final int treackletartevalue = 12;
final int bbefbvalue = 14;
final int chocolatefrogsvalue = 8;

/*enum foodType {
  drink,
  meal,
  desert,
  soup,
}*/

/*final List<Map> foods = [
  {'food': 'Drinks', 'icon': Icons.local_drink},
  {'food': 'Meals', 'icon': Icons.fastfood},
  {'food': 'Deserts', 'icon': Icons.local_drink},
  {'food': 'Soups', 'icon': Icons.local_drink}
];

final List<Map<String, IconData>> foodIcons = [
  {'icon': Icons.local_drink},
  {'icon': Icons.fastfood},
  {'icon': Icons.local_drink},
  {'icon': Icons.local_drink}
];*/

int getTotalItemPrice(List<int> item) {
  int price = 0;

  for (int i in item) {
    price += i;
  }
  return price;
}

int gettotalprice() {
  int _total = 0;
  _total = getTotalItemPrice(totaldrinks) +
      getTotalItemPrice(totalsoups) +
      getTotalItemPrice(totalmeals) +
      getTotalItemPrice(totaldesserts);
  print("total drinks price:  ${getTotalItemPrice(totaldrinks)}");
  print("total soups price:  ${getTotalItemPrice(totalsoups)}");
  print("total meals price:  ${getTotalItemPrice(totalmeals)}");
  print("total desserts price:  ${getTotalItemPrice(totaldesserts)}");

  return _total;
}
