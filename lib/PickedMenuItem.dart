import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'globals.dart' as globals;

class Menu extends StatefulWidget {
  String food;
  Menu([this.food]);
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  @override
  Widget build(BuildContext context) {
    if (widget.food == "Drinks") {
      return DrinksPage();
    } else if (widget.food == "Soups") {
      return SoupsPage();
    } else if (widget.food == "Meals") {
      return MealsPage();
    } else if (widget.food == "Desserts") {
      return DessertsPage();
    }
  }
}

class DrinksPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text("Drinks page"),
      ),
      body: Drinksitems(),
    );
  }
}

class SoupsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text("Soups page"),
      ),
      body: Soupsitems(),
    );
  }
}

class MealsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text("Meals page"),
      ),
      body: Mealsitems(),
    );
  }
}

class DessertsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text("Desserts page"),
      ),
      body: Dessertsitems(),
    );
  }
}

class FirstRoute extends StatelessWidget {
  int _total;
  int gettotalprice() {
    _total = globals.drinkscounter +
        globals.soupscounter +
        globals.mealscounter +
        globals.dessertscounter;
    return _total;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AniMenu'),
        centerTitle: true,
        actions: <Widget>[],
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 60,
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: Text(
                        'your total drinks price is: \$${globals.getTotalItemPrice(globals.totaldrinks)}',
                        style: TextStyle(fontSize: 25.0))),
              ],
            ),
            SizedBox(
              height: 60,
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: Text(
                        'your total soups price is: \$${globals.getTotalItemPrice(globals.totalsoups)}',
                        style: TextStyle(fontSize: 25.0))),
              ],
            ),
            SizedBox(
              height: 60,
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: Text(
                        'your total meals price is: \$${globals.getTotalItemPrice(globals.totalmeals)}',
                        style: TextStyle(fontSize: 25.0))),
              ],
            ),
            SizedBox(
              height: 60,
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: Text(
                        'your total desserts price is: \$${globals.getTotalItemPrice(globals.totaldesserts)}',
                        style: TextStyle(fontSize: 25.0))),
              ],
            ),
            SizedBox(
              height: 60,
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: Text(
                        'Your total price is: \$${globals.gettotalprice()}',
                        style: TextStyle(fontSize: 40.0))),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class Drinksitems extends StatefulWidget {
  Drinksitems({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _DrinksitemsState createState() => _DrinksitemsState();
}

class _DrinksitemsState extends State<Drinksitems> {
  int _butterbeercounter = 0;
  int _unicornbloodcounter = 0;
  int _hitmanshotcounter = 0;

  int _Drinkscounter;
  int _total;

  int getDrinksCounter() {
    _Drinkscounter = _butterbeercounter * globals.butterbeervalue +
        _unicornbloodcounter * globals.unicornbloodvalue +
        _hitmanshotcounter * globals.hitmanshotevalue;
    return _Drinkscounter;
  }

  void butterbeeradd() {
    setState(() {
      _butterbeercounter++;
    });
  }

  void butterbeerminus() {
    setState(() {
      if (_butterbeercounter != 0) _butterbeercounter--;
    });
  }

  void unicornbloodadd() {
    setState(() {
      _unicornbloodcounter++;
    });
  }

  void unicornbloodminus() {
    setState(() {
      if (_unicornbloodcounter != 0) _unicornbloodcounter--;
    });
  }

  void hitmanshotadd() {
    setState(() {
      _hitmanshotcounter++;
    });
  }

  void hitmanshotminus() {
    setState(() {
      if (_hitmanshotcounter != 0) _hitmanshotcounter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 10, top: 20),
        child: Center(
          child: ListView(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                          'Please pick from the amazing drinks below to devour. If you click on the picture you can see the ingredients.',
                          style: TextStyle(fontSize: 20.0))),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/Butterbeer.jpg'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _butterbeeralert(context)),
                  Expanded(
                      child: Text('ButterBeer',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn1",
                    onPressed: butterbeeradd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_butterbeercounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn2",
                    onPressed: butterbeerminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/Unicornblood.jpg'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _unicornbloodalert(context)),
                  Expanded(
                      child: Text('Unicorn Blood',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn3",
                    onPressed: unicornbloodadd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_unicornbloodcounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn4",
                    onPressed: unicornbloodminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/HitmanShot.png'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _hitmanshotalert(context)),
                  Expanded(
                      child: Text('Hitman Shot',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn5",
                    onPressed: hitmanshotadd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_hitmanshotcounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn6",
                    onPressed: hitmanshotminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                          'Your drinks amount now is: \$${getDrinksCounter()}',
                          style: TextStyle(fontSize: 20.0))),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      onPressed: () {
                        globals.drinkscounter = getDrinksCounter();
                        globals.totaldrinks.add(globals.drinkscounter);
                        print(globals.totaldrinks.toString());
                        Navigator.pop(context);
                      },
                      child:
                          Text('Order', style: new TextStyle(fontSize: 18.0)),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Soupsitems extends StatefulWidget {
  Soupsitems({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SoupsitemsState createState() => _SoupsitemsState();
}

class _SoupsitemsState extends State<Soupsitems> {
  int _narutoramencounter = 0;
  int _creamstewcounter = 0;
  int _misosoupcounter = 0;

  int _Soupscounter;
  int _total;

  int getSoupsCounter() {
    _Soupscounter = _narutoramencounter * globals.narutoramenevalue +
        _creamstewcounter * globals.creamstewevalue +
        _misosoupcounter * globals.misosoupevalue;
    return _Soupscounter;
  }

  void narutoramenadd() {
    setState(() {
      _narutoramencounter++;
    });
  }

  void narutoramenminus() {
    setState(() {
      if (_narutoramencounter != 0) _narutoramencounter--;
    });
  }

  void creamstewadd() {
    setState(() {
      _creamstewcounter++;
    });
  }

  void creamstewminus() {
    setState(() {
      if (_creamstewcounter != 0) _creamstewcounter--;
    });
  }

  void misosoupadd() {
    setState(() {
      _misosoupcounter++;
    });
  }

  void misosoupminus() {
    setState(() {
      if (_misosoupcounter != 0) _misosoupcounter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 10, top: 20),
        child: Center(
          child: ListView(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                          'Please pick from the mouth-watering Soups below to devour. If you click on the picture you can see the ingredients.',
                          style: TextStyle(fontSize: 20.0))),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/Ramen.jpg'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _narutoramenalert(context)),
                  Expanded(
                      child: Text('Naruto Ramen',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn1",
                    onPressed: narutoramenadd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_narutoramencounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn2",
                    onPressed: narutoramenminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/CreamStew.png'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _creamstewalert(context)),
                  Expanded(
                      child: Text('Cream Stew',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn3",
                    onPressed: creamstewadd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_creamstewcounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn4",
                    onPressed: creamstewminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/MisoSoup.png'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _misosoupalert(context)),
                  Expanded(
                      child: Text('Miso Soup',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn5",
                    onPressed: misosoupadd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_misosoupcounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn6",
                    onPressed: misosoupminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                          'Your soups amount now is: \$${getSoupsCounter()}',
                          style: TextStyle(fontSize: 20.0))),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      onPressed: () {
                        globals.soupscounter = getSoupsCounter();
                        globals.totalsoups.add(globals.soupscounter);
                        print(globals.totalsoups.toString());
                        Navigator.pop(context);
                      },
                      child:
                          Text('Order', style: new TextStyle(fontSize: 18.0)),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Mealsitems extends StatefulWidget {
  Mealsitems({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MealsitemsState createState() => _MealsitemsState();
}

class _MealsitemsState extends State<Mealsitems> {
  int _dragonstewcounter = 0;
  int _bleachcutcounter = 0;
  int _killermackerelcounter = 0;

  int _Mealscounter;
  int _total;

  int getMealsCounter() {
    _Mealscounter = _dragonstewcounter * globals.dragonstewvalue +
        _bleachcutcounter * globals.bleachcutvalue +
        _killermackerelcounter * globals.killermackerelvalue;
    return _Mealscounter;
  }

  void dragonstewadd() {
    setState(() {
      _dragonstewcounter++;
    });
  }

  void dragonstewminus() {
    setState(() {
      if (_dragonstewcounter != 0) _dragonstewcounter--;
    });
  }

  void bleachcutadd() {
    setState(() {
      _bleachcutcounter++;
    });
  }

  void bleachcutminus() {
    setState(() {
      if (_bleachcutcounter != 0) _bleachcutcounter--;
    });
  }

  void killermackereladd() {
    setState(() {
      _killermackerelcounter++;
    });
  }

  void killermackerelminus() {
    setState(() {
      if (_killermackerelcounter != 0) _killermackerelcounter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 10, top: 20),
        child: Center(
          child: ListView(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                          'Please pick from the divine meals below to devour. If you click on the picture you can see the ingredients.',
                          style: TextStyle(fontSize: 20.0))),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/DragonStew.png'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _dragonstewalert(context)),
                  Expanded(
                      child: Text('Dragon Stew',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn1",
                    onPressed: dragonstewadd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_dragonstewcounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn2",
                    onPressed: dragonstewminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/BleachCut.jpg'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _bleachcutalert(context)),
                  Expanded(
                      child: Text('Bleach Cut',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn3",
                    onPressed: bleachcutadd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_bleachcutcounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn4",
                    onPressed: bleachcutminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/KillerMackerel.png'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _killermackerelalert(context)),
                  Expanded(
                      child: Text('Killer Mackerel',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn5",
                    onPressed: killermackereladd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_killermackerelcounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn6",
                    onPressed: killermackerelminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                          'Your meals amount now is: \$${getMealsCounter()}',
                          style: TextStyle(fontSize: 20.0))),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      onPressed: () {
                        globals.mealscounter = getMealsCounter();
                        globals.totalmeals.add(globals.mealscounter);
                        print(globals.totalmeals.toString());
                        Navigator.pop(context);
                      },
                      child:
                          Text('Order', style: new TextStyle(fontSize: 18.0)),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Dessertsitems extends StatefulWidget {
  Dessertsitems({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _DessertsitemsState createState() => _DessertsitemsState();
}

class _DessertsitemsState extends State<Dessertsitems> {
  int _treacletartcounter = 0;
  int _bbefbcounter = 0;
  int _chocolatefrogscounter = 0;

  int _Dessertscounter;
  int _total;

  int getDessertsCounter() {
    _Dessertscounter = _treacletartcounter * globals.treackletartevalue +
        _bbefbcounter * globals.bbefbvalue +
        _chocolatefrogscounter * globals.chocolatefrogsvalue;
    return _Dessertscounter;
  }

  void treacletartadd() {
    setState(() {
      _treacletartcounter++;
    });
  }

  void treacletartminus() {
    setState(() {
      if (_treacletartcounter != 0) _treacletartcounter--;
    });
  }

  void bbefbadd() {
    setState(() {
      _bbefbcounter++;
    });
  }

  void bbefbminus() {
    setState(() {
      if (_bbefbcounter != 0) _bbefbcounter--;
    });
  }

  void chocolatefrogsadd() {
    setState(() {
      _chocolatefrogscounter++;
    });
  }

  void chocolatefrogsminus() {
    setState(() {
      if (_chocolatefrogscounter != 0) _chocolatefrogscounter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 10, top: 20),
        child: Center(
          child: ListView(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                          'Please pick from the lovely desserts below to devour. If you click on the picture you can see the ingredients.',
                          style: TextStyle(fontSize: 20.0))),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/Treacletart.jpg'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _treackletartalert(context)),
                  Expanded(
                      child: Text('Treacle Tart',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn1",
                    onPressed: treacletartadd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_treacletartcounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn2",
                    onPressed: treacletartminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/BertieBottsEFB.png'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _bbefbalert(context)),
                  Expanded(
                      child: Text('Bertie Botts Every Flavour Beans',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn3",
                    onPressed: bbefbadd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_bbefbcounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn4",
                    onPressed: bbefbminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                children: <Widget>[
                  IconButton(
                      icon: Image.asset('images/ChocolateFrogs.jpg'),
                      padding: const EdgeInsets.all(0.0),
                      iconSize: 150,
                      onPressed: () => _chocolatefrogsalert(context)),
                  Expanded(
                      child: Text('Chocolate Frogs',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold))),
                  new FloatingActionButton(
                    heroTag: "btn5",
                    onPressed: chocolatefrogsadd,
                    child: new Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    backgroundColor: Colors.redAccent,
                  ),
                  new Text('$_chocolatefrogscounter',
                      style: new TextStyle(fontSize: 30.0)),
                  new FloatingActionButton(
                    heroTag: "btn6",
                    onPressed: chocolatefrogsminus,
                    child: new Icon(
                        const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                        color: Colors.black),
                    backgroundColor: Colors.redAccent,
                  ),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                          'Your meals amount now is: \$${getDessertsCounter()}',
                          style: TextStyle(fontSize: 20.0))),
                ],
              ),
              SizedBox(
                height: 60.0,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      onPressed: () {
                        globals.dessertscounter = getDessertsCounter();
                        globals.totaldesserts.add(globals.dessertscounter);
                        print(globals.totaldesserts.toString());
                        Navigator.pop(context);
                      },
                      child:
                          Text('Order', style: new TextStyle(fontSize: 18.0)),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

_butterbeeralert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc: "Cream Soda, Whipped Cream, Brown Sugar",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}

_unicornbloodalert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc: "Raspberries, Icing Sugar, Water",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}

_hitmanshotalert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc: "Espresso",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}

_narutoramenalert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc: "Eggs, Onions, Beef, Noodles",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}

_creamstewalert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc: "Chicken Thighs, Onions, Carrots, Potatoes, Cream Cheese",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}

_misosoupalert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc: "Dashi Granules, Water, Miso Paste, Silken Tofu, Green Onions",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}

_dragonstewalert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc: "Beef, Potatoes, Beef Bouillon, Carrots, Celery",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}

_bleachcutalert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc: "Tenderloin Steak, Cheese",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}

_killermackerelalert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc: "Macekerel Fish, Wasabi",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}

_treackletartalert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc: "Shortcrust pastry, golden syrup, breadcrumbs, lemon juice",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}

_bbefbalert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc:
        "Banana, Black Pepper, Blueberry, Booger, Candyfloss, Cherry, Cinnamon, Dirt, Earthworm, Earwax, Grass, Green Apple, Marshmallow, Rotten Egg, Sausage, Lemon, Soap, Tutti-Fruitti, Vomit and Watermelon",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}

_chocolatefrogsalert(context) {
  Alert(
    context: context,
    title: "Ingredients",
    desc: "Chocolate, Milk",
    buttons: [
      DialogButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
        onPressed: () => Navigator.pop(context),
        width: 150,
      )
    ],
  ).show();
}
